//导入vue和vue路由
import Vue from 'vue'
import Router from 'vue-router'
//导入element ui和element ui的css样式
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
//加载element ui组件和vue的路由组件
Vue.use(ElementUI);
Vue.use(Router)

//导入登录页面
import login from '../components/loginRegister'
//用户完善信息页面
import user from '../components/user-system'
//导航
import home from '../components/home-service'
//主页
import index from '../components/index'
//商品品牌组件
import brand from '../components/home/brand'
//商品类别组件
import type from '../components/home/type'
//个人中心组件
import personal from '../components/home/personal'
//属性编辑组件
import attr from '../components/home/attr'
//属性添加组件
import addAttr from '../components/home/addAttr'
//新增商品组件
import addGoods from '../components/goods/addGoods'
//商品展示组件
import goodsIndex from '../components/goods/goods-index'
export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      title:"登录",
      component: login
    },
    {
      path: '/user',
      name: 'user',
      title:"完善个人信息",
      component: user
    },
    {
      path: '/home',
      name: 'home',
      title:"导航栏",
      component: home,
      children: [
        {
          path: '/index',
          name: 'index',
          title:"主页",
          component: index,
        },
        {
          path: '/brand',
          name: 'brand',
          title:"品牌管理",
          component: brand,
        },
        {
          path: '/type',
          name: 'type',
          title:"类别管理",
          component: type,
        },
        {
          path: '/personal',
          name: 'personal',
          title:"个人信息",
          component: personal,
        },
        {
          path: '/attr',
          name: 'attr',
          title:"属性列表",
          component: attr,
        },
        {
          path: '/addAttr',
          name: 'addAttr',
          title:"属性添加",
          component: addAttr,
        },
        {
          path: '/addGoods',
          name: 'addGoods',
          title:"商品添加",
          component: addGoods,
        },
        {
          path: '/goodsIndex',
          name: 'goodsIndex',
          title:"商品查询",
          component: goodsIndex,
        }
      ]
    }
  ]
})
