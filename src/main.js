// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false
import axios from 'axios'
//每次的axios跨域请求 使用当前浏览器的cookie
//因为目前的登录调用了JWT接口 所以不用设置cookie
//axios.defaults.withCredentials = true
import qs from 'qs'
/* 声明全局的qs   */
Vue.prototype.$qs = qs
/*  声明全局的axios  */
Vue.prototype.$gmm = axios
/* eslint-disable no-new */

//路由守卫
router.beforeEach((to, from, next) => {
  //不需要拦截的 去登录路径不需要拦截 直接放开
  if (to.path == "/") {
    next();
  }
  //判断是否填写了真实名称 如果没有填写 除了完善个人信息页面和登录页面 不能去其他页面
  var realName = qs.parse(localStorage.getItem("user")).realName
  //没有完善个人信息
  if (realName == null || realName == "") {
    //从登录页面去完善个人信息页面的放行
    if (from.path == "/" && to.path == "/user") {
      next()
      return;
    }
    //如果想去其他页面 去登录页面放行 但是去别的页面打回到完善个人信息页面
    if (from.path == "/user") {
      if (to.path == "/") {
        next();
        return;
      }
      next("/user");
      return;
    }
    next("/")
    return
  }
  //如果完善过个人信息 就不能再访问完善个人信息页面
  if (realName != null) {
    //想跳转到个人信息页面的一律打回到主页
    if (to.path == "/user") {
      next("/home");
      return;
    }
  }
  //需要拦截的
  //判断用户是否登陆   判断localStorage是否有值
  if (localStorage.getItem("userToken") != null && localStorage.getItem("userToken") != "") {
    next();
    return;
  }
  next("/");
})

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
